﻿using System;
using System.IO;
using System.Windows.Forms;

namespace labFileExplorer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            CurDir = "C:/";

            this.KeyPreview = true;
            this.KeyPress += (s, e) => { if (e.KeyChar == (char)8) GoToDir(); };

            buUp.Click += (s, e) => GoToDir();


            miViewLargeIcon.Click += (s, e) => lv.View = View.LargeIcon;
            miViewSmallIcon.Click += (s, e) => lv.View = View.SmallIcon;
            miViewList.Click += (s, e) => lv.View = View.List;
            miViewDetails.Click += (s, e) => lv.View = View.Details;
            miViewTitle.Click += (s, e) => lv.View = View.Tile;

            tv.Click += Tv_Click;
            tv.DoubleClick += Tv_DoubleClick;
            tv.ContextMenuStrip = contextMenuStrip1;

            lv.ItemSelectionChanged += Lv_ItemSelectionChanged;
            lv.DoubleClick += Lv_DoubleClick;
            lv.ContextMenuStrip = contextMenuStrip1;

            MiPopupProperties.Click += (s, e) => LoadDir(SelItem);
            MiPopupDel.Click += MiPopupDel_Click;
            MiPopupProperties.Click += MiPopupProperties_Click;


            //lv.Columns.Add(new ColumnHeader() { Text = "Имя", Width = 400 });

            lv.Columns.Add("Имя", 400);
            lv.Columns.Add("Дата изменения", 150);
            lv.Columns.Add("Тип", 100);
            lv.Columns.Add("Размер", 150);

            LoadDir(CurDir);

            foreach (var item in Directory.GetLogicalDrives())
            {
                tv.Nodes.Add("", item, 2);
            }

            //HW
            //Добработать кнопку buUp чтобы не было ошибки когда выходишь выше диска (Выполнено)
            //По кнопке backspace переходить на уровень выше [buUp] (Выполнено)
            //Добавить кнопку "создать папку" (+-)
            //Добавить кнопку "показать \ скрыть скрытые папки и файлы" (+-)
            //Добавить разные иконки на разные типы файлов [item.Extension]
            //Добавить панель с кнопками картинка и название диска чтобы можно было по кнопке на один из дисков перепрыгнуть [Directory.GetLogicalDrives]


            //HW 2
            //Размер файлов отображать в формате 000 000 000
            //Добавить кнопку выбора вида отображения размера файлов (байт, КБ, МБ, ГБ или автоматически)
            //Доработать столбец тип файлов (Выполнено)
            //Доработать отображение данных в TreeView (скрытые папки, иконки, системные папка, ...)
            //Добавить различные иконки в TreeView (DVD, HDD, системные папка, пустые папки, ...)
            //Доработать форму Свойства файла
            //Создать форму Свойства папки
            //По кнопке del удалить файлы/папки с предупреждением
            //Доработать строку состояния
            //Доработать механизм переходов buBack и buForward (например, через два стека)
        }

        private void GoToDir()
        {
            if (Directory.GetParent(CurDir) != null)
            {
                LoadDir(Directory.GetParent(CurDir).ToString());
            }
        }

        private void MiPopupProperties_Click(object sender, EventArgs e)
        {
            if (File.Exists(SelItem))
            {
                var f = new FmFileProperties(SelItem);
                f.ShowDialog();
            }
            else if (Directory.Exists(SelItem))
            {

            }
        }

        private void MiPopupDel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Удалить?",
                Application.ProductName,
                MessageBoxButtons.YesNoCancel) == DialogResult.Yes)
            {
                MessageBox.Show($"Файл {SelItem} удален");
            }
        }

        private void Tv_DoubleClick(object sender, EventArgs e)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(tv.SelectedNode.FullPath);
            tv.BeginUpdate();
            tv.SelectedNode.Nodes.Clear();
            foreach (var item in directoryInfo.GetDirectories())
            {
                tv.SelectedNode.Nodes.Add("", item.Name, 1);
                tv.EndUpdate();
                LoadDir(tv.SelectedNode.FullPath);
            }
        }

        private void Tv_Click(object sender, EventArgs e)
        {
            LoadDir(tv.SelectedNode.FullPath);
        }

        private void Lv_DoubleClick(object sender, EventArgs e)
        {
            LoadDir(SelItem);
        }

        private void Lv_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            SelItem = Path.Combine(CurDir, e.Item.Text);
            laStatus.Text = $"Элементов : {lv.Items.Count}, Выбранно: {lv.SelectedItems.Count}, (общийразмер)";
        }

        private void LoadDir(string newDir)

        {
            DirectoryInfo directoryInfo = new DirectoryInfo(newDir);
            lv.BeginUpdate();
            lv.Items.Clear();
            foreach (var item in directoryInfo.GetDirectories())
            {
                //lv.Items.Add(item.Name, 0);
                lv.Items.Add(new ListViewItem(
                    new string[] { item.Name, item.LastWriteTime.ToString(), "Папка", "" },
                    0
                    ));
                //МЕНЯТЬ ЗДЕСЬ ПРИЗНАК СКРЫТОСТИ Item.атрибут

            }
            foreach (var item in directoryInfo.GetFiles())
            {
                lv.Items.Add(new ListViewItem(
                    new string[] { item.Name, item.LastWriteTime.ToString(), item.Extension.TrimStart('.').ToUpper() + " Файл", $"{item.Length} байт" },
                    0
                    ));

                //lv.Items.Add(item.Name, 1);
            }
            lv.EndUpdate();
            CurDir = newDir;
            edDir.Text = newDir;
        }

        public string CurDir { get; private set; }
        public string SelItem { get; private set; }

    }
}
