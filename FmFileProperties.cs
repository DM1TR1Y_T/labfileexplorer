﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labFileExplorer
{
    public partial class FmFileProperties : Form
    {
        public FmFileProperties(string filename)
        {
            InitializeComponent();

            var x = new FileInfo(filename);
            this.Text = $"Файл: {x.Name}, {x.Extension}, {x.Length}";
        }
    }
}
