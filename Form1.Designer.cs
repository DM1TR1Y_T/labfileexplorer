﻿
namespace labFileExplorer
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.buUp = new System.Windows.Forms.ToolStripButton();
            this.edDir = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.miViewLargeIcon = new System.Windows.Forms.ToolStripMenuItem();
            this.miViewSmallIcon = new System.Windows.Forms.ToolStripMenuItem();
            this.miViewList = new System.Windows.Forms.ToolStripMenuItem();
            this.miViewDetails = new System.Windows.Forms.ToolStripMenuItem();
            this.miViewTitle = new System.Windows.Forms.ToolStripMenuItem();
            this.tv = new System.Windows.Forms.TreeView();
            this.iILargeIcons = new System.Windows.Forms.ImageList(this.components);
            this.lv = new System.Windows.Forms.ListView();
            this.iISmallIcons = new System.Windows.Forms.ImageList(this.components);
            this.iITreeView = new System.Windows.Forms.ImageList(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.MiPopupProperties = new System.Windows.Forms.ToolStripMenuItem();
            this.MiPopupDel = new System.Windows.Forms.ToolStripMenuItem();
            this.MiPopupCreate = new System.Windows.Forms.ToolStripMenuItem();
            this.MiPopupShowOrHide = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.laStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.toolStrip1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton2,
            this.buUp,
            this.edDir,
            this.toolStripDropDownButton1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1264, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "←";
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton2.Text = "→";
            // 
            // buUp
            // 
            this.buUp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buUp.Image = ((System.Drawing.Image)(resources.GetObject("buUp.Image")));
            this.buUp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buUp.Name = "buUp";
            this.buUp.Size = new System.Drawing.Size(23, 22);
            this.buUp.Text = "↑";
            // 
            // edDir
            // 
            this.edDir.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.edDir.Name = "edDir";
            this.edDir.Size = new System.Drawing.Size(1100, 25);
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miViewLargeIcon,
            this.miViewSmallIcon,
            this.miViewList,
            this.miViewDetails,
            this.miViewTitle});
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(45, 22);
            this.toolStripDropDownButton1.Text = "View";
            // 
            // miViewLargeIcon
            // 
            this.miViewLargeIcon.Name = "miViewLargeIcon";
            this.miViewLargeIcon.Size = new System.Drawing.Size(134, 22);
            this.miViewLargeIcon.Text = "Large Icons";
            // 
            // miViewSmallIcon
            // 
            this.miViewSmallIcon.Name = "miViewSmallIcon";
            this.miViewSmallIcon.Size = new System.Drawing.Size(134, 22);
            this.miViewSmallIcon.Text = "Small Icons";
            // 
            // miViewList
            // 
            this.miViewList.Name = "miViewList";
            this.miViewList.Size = new System.Drawing.Size(134, 22);
            this.miViewList.Text = "List";
            // 
            // miViewDetails
            // 
            this.miViewDetails.Name = "miViewDetails";
            this.miViewDetails.Size = new System.Drawing.Size(134, 22);
            this.miViewDetails.Text = "Details";
            // 
            // miViewTitle
            // 
            this.miViewTitle.Name = "miViewTitle";
            this.miViewTitle.Size = new System.Drawing.Size(134, 22);
            this.miViewTitle.Text = "Titles";
            // 
            // tv
            // 
            this.tv.CausesValidation = false;
            this.tv.Dock = System.Windows.Forms.DockStyle.Left;
            this.tv.ImageIndex = 0;
            this.tv.ImageList = this.iILargeIcons;
            this.tv.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.tv.Indent = 131;
            this.tv.ItemHeight = 128;
            this.tv.Location = new System.Drawing.Point(0, 25);
            this.tv.Margin = new System.Windows.Forms.Padding(2);
            this.tv.Name = "tv";
            this.tv.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tv.SelectedImageIndex = 0;
            this.tv.Size = new System.Drawing.Size(185, 656);
            this.tv.TabIndex = 1;
            // 
            // iILargeIcons
            // 
            this.iILargeIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("iILargeIcons.ImageStream")));
            this.iILargeIcons.TransparentColor = System.Drawing.Color.Transparent;
            this.iILargeIcons.Images.SetKeyName(0, "62916-file-folder-icon.png");
            // 
            // lv
            // 
            this.lv.HideSelection = false;
            this.lv.LargeImageList = this.iILargeIcons;
            this.lv.Location = new System.Drawing.Point(195, 25);
            this.lv.Margin = new System.Windows.Forms.Padding(2);
            this.lv.Name = "lv";
            this.lv.Size = new System.Drawing.Size(1069, 632);
            this.lv.TabIndex = 2;
            this.lv.UseCompatibleStateImageBehavior = false;
            // 
            // iISmallIcons
            // 
            this.iISmallIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("iISmallIcons.ImageStream")));
            this.iISmallIcons.TransparentColor = System.Drawing.Color.Transparent;
            this.iISmallIcons.Images.SetKeyName(0, "file-rounded-outlined-symbol_icon-icons.com_73252.png");
            // 
            // iITreeView
            // 
            this.iITreeView.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("iITreeView.ImageStream")));
            this.iITreeView.TransparentColor = System.Drawing.Color.Transparent;
            this.iITreeView.Images.SetKeyName(0, "40097.png");
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MiPopupProperties,
            this.MiPopupDel,
            this.MiPopupCreate,
            this.MiPopupShowOrHide,
            this.toolStripMenuItem3});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(260, 114);
            // 
            // MiPopupProperties
            // 
            this.MiPopupProperties.Name = "MiPopupProperties";
            this.MiPopupProperties.Size = new System.Drawing.Size(259, 22);
            this.MiPopupProperties.Text = "Properties";
            // 
            // MiPopupDel
            // 
            this.MiPopupDel.Name = "MiPopupDel";
            this.MiPopupDel.Size = new System.Drawing.Size(259, 22);
            this.MiPopupDel.Text = "Delete";
            // 
            // MiPopupCreate
            // 
            this.MiPopupCreate.Name = "MiPopupCreate";
            this.MiPopupCreate.Size = new System.Drawing.Size(259, 22);
            this.MiPopupCreate.Text = "Create folder";
            // 
            // MiPopupShowOrHide
            // 
            this.MiPopupShowOrHide.Name = "MiPopupShowOrHide";
            this.MiPopupShowOrHide.Size = new System.Drawing.Size(259, 22);
            this.MiPopupShowOrHide.Text = "Show/Hide hidden files and folders";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(259, 22);
            this.toolStripMenuItem3.Text = "toolStripMenuItem3";
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.laStatus});
            this.statusStrip1.Location = new System.Drawing.Point(195, 659);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 10, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1069, 22);
            this.statusStrip1.TabIndex = 5;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // laStatus
            // 
            this.laStatus.Name = "laStatus";
            this.laStatus.Size = new System.Drawing.Size(118, 17);
            this.laStatus.Text = "toolStripStatusLabel1";
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(185, 25);
            this.splitter1.Margin = new System.Windows.Forms.Padding(2);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(10, 656);
            this.splitter1.TabIndex = 3;
            this.splitter1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 681);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.lv);
            this.Controls.Add(this.tv);
            this.Controls.Add(this.toolStrip1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton buUp;
        private System.Windows.Forms.ToolStripTextBox edDir;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem miViewLargeIcon;
        private System.Windows.Forms.ToolStripMenuItem miViewSmallIcon;
        private System.Windows.Forms.ToolStripMenuItem miViewList;
        private System.Windows.Forms.ToolStripMenuItem miViewDetails;
        private System.Windows.Forms.ToolStripMenuItem miViewTitle;
        private System.Windows.Forms.TreeView tv;
        private System.Windows.Forms.ListView lv;
        private System.Windows.Forms.ImageList iILargeIcons;
        private System.Windows.Forms.ImageList iISmallIcons;
        private System.Windows.Forms.ImageList iITreeView;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem MiPopupProperties;
        private System.Windows.Forms.ToolStripMenuItem MiPopupDel;
        private System.Windows.Forms.ToolStripMenuItem MiPopupCreate;
        private System.Windows.Forms.ToolStripMenuItem MiPopupShowOrHide;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel laStatus;
        private System.Windows.Forms.Splitter splitter1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
    }
}

